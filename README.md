# manjaro i3 config

#### 电脑配置
iMac 27-inch Late 2012
固件版本：	Broadcom BCM43xx 1.0 (7.21.190.33 AirPortDriverBrcm4360-1415)

Linux 下被识别为 Broadcom BCM4331

Deepin 可以被正常驱动，并支持5G，偶尔会断连 但是不影响使用。

Manjaro下 需要手动安装 相应内核版本的broadcom-wl

Archlinux 安装时需要先插网线安装，然后手动安装broadcom-wl驱动。

Windows 下貌似被识别成了BCM94331  :sweat_smile: 

当暂时无法通过无线网卡联网时,可以通过手机热点更新,iPhone可行.
```
sudo pacman -Syyu
sudo pacman -S linux54-broadcom-wl
```

声音 speaker-test 如果不能听到声音 可以手动调节 声音首选项

部分字体

复制字体文件到/usr/share/fonts/或者~/.local/share/fonts/中，执行fc-cache -fv（f参数：force，v参数：view）

#### 介绍
主要包含 alacritty fish i3config i3status pacman.conf mirrorlist  

#### 软件架构
x64

#### 使用说明

```
git clone http://gitee.com/humanfans/imac.git 
cd config
sudo mv -f pacman.conf /etc #添加腾讯ArchlinuxCN源，开启YAY颜色标记
sudo mv -f mirrorlist /etc/pacman.d #添加华为云Manjaro源
sudo pacman -Syyu && sudo pacman -S archlinuxcn-keyring #更新及安装ArchlinuxCNkey
```

#### 软件安装
kvm虚拟机参考
[安装KVM、QEMU和Virt Manager](https://ywnz.com/linuxjc/4953.html)

vmware workstaton Pro
[Manjaro安装VMwareWorkstation](https://www.jianshu.com/p/de571b44ab60)

远程软件 teamviewer

md编辑软件 typora

文本编辑软件 mousepad

终端模拟器  fish 配置oh my fish

[oh-my-fish On github](https://github.com/oh-my-fish/oh-my-fish)

[oh-my-fish On gitee](https://gitee.com/humanfans/oh-my-fish)

gitee 安装方法为离线安装

```
$ git clone https://gitee.com/humanfans/oh-my-fish.git
$ cd oh-my-fish
$ bin/install --offline
```


alacritty 配色 

配置 alacritty 
高斯模糊 字体改为微软的Cascadia Code字体了。

#### 引导

在Mac系统 恢复模式下 安装的rEFind https://www.rodsbooks.com/refind/

目前是Mac Windows10 Archlinux 3系统安装。
